import os
import time
from celery import Celery
from django.core.mail import EmailMessage
from django.forms.models import model_to_dict

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'notifier.settings')
app = Celery('notifier')
app.conf.update(timezone='Asia/Bishkek')
app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


@app.task()
def notify_users(users, event):
    for user in users:
        email = EmailMessage(subject=event['title'], body=event['info'], to=[user['email']])
        email.send()

@app.task()
def schedule_notifications(event_id):
    from events.models import Event
    if Event.objects.filter(id=event_id).exists():
        event = Event.objects.get(id=event_id)

        from django.contrib.auth.models import User
        users = User.objects.all()
        users = [model_to_dict(i) for i in users]
        users = [users[i:(i+1)*100]
                if i==0 else users[i*100:(i+1)*100]
                for i in range(len(users[::100]))]
        for hun_users in users:
            notify_users.delay(hun_users, model_to_dict(event))
            time.sleep(60*60)
        event.is_notified = True
        event.save()
