from events.models import Event
from notifier.tasks import schedule_notifications


class EventService:
    model = Event

    @classmethod
    def create(cls, title: str, info: str,
                    data: str) -> Event:
        event = cls.model.objects.create(
            title=title,
            info=info,
            data=data
        )
        schedule_notifications.apply_async(args=[event.id], eta=event.data)
        return event
