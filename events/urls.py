from django.urls import path
from events import views


urlpatterns = [
    path('', views.EventAPIView.as_view({'post': 'retrieve'}), name='event'),
]
