from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from events.serializers import EventSerializer
from events.services import EventService


class EventAPIView(ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = EventSerializer

    def retrieve(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        EventService.create(
            title=serializer.validated_data['title'],
            info=serializer.validated_data['info'],
            data=serializer.validated_data['data'],
        )
        return Response(data={
            'message': 'The user has successfully registered',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)
