from django.contrib import admin
from events.models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'data', 'is_notified')
    list_display_links = ('title',)
    list_filter = ('title', 'data')
    fields = ('title', 'info', 'data')
