from django.db import models


class Event(models.Model):
    data = models.DateTimeField(verbose_name='Дата проведения')
    title = models.CharField(max_length=250, verbose_name='Наименование события')
    info = models.TextField(verbose_name='Информация о событии')
    is_notified = models.BooleanField(default=False, verbose_name='Уведомлено')

    class Meta:
        db_table = 'events'
        verbose_name = 'Событие'
        verbose_name_plural = 'События'
        ordering = ('-data',)
